# Sca Event Planning Example

Example of how to use a hosted git repository to facilitate planning an event.

NOTICE!! The "Issues" function here is locked. If you want to submit feedback/bug reports/enhancements, there's a [project](https://gitlab.com/cordelya/event-planning-planning) for that.

PR's are welcomed.

## What is this for? ##
This repository demonstrates the use of a Gitlab (or other hosted) repository and associated tools to facilitate tracking the planning process for an event. This particular repository is built with [SCA](https://sca.org) events in mind, but you may still find it useful for planning other events.

## How can I use it? ##
If you have a Gitlab account, you can "fork" this repository to your own account. A fork is a copy of a different repository that has its own history that diverges from the history of the originating repository starting when the fork is created. So, anything done to the original repository before the fork is created will also appear in the fork, and anything done to the original after the fork is created will not appear in the fork. Anything created in the fork will not appear in the originating repository.

Once you have forked the repository, if you're using the fork to plan an event, remove the fork relationship so the originating repository doesn't receive merge requests with your event-specific changes.

The Issues tool will not map over on its own, so as a work-around, we'll export the issues to a csv file that you can import into your copy. You may want to load the csv into a spreadsheet application, edit the date fields so they make sense for your event (set creation dates to the date you're starting the project), and then export the information back to csv, at which point you can upload the modified Issues data into your project.

Grabbing the Wiki example pages will either be more challenging or more time consuming, depending on which route you take:

1. Easier, more time consuming: copy and paste the text from each page of the wiki in this project into your new project
2. Harder, quicker: git clone the wiki (similar url to cloning the main repository, but ends in `.wiki` instead of `.git`), edit the remote to point at your project's remote, and push. (Untested - this may require you to clone both the originating and destination wikis and copy files over, commit, then push.)

## Recommendations ##
Plan to designate a person whose job is purely project management. 

* This is not a decision-making role in terms of making decisions for the event, so assigning this role to someone who is not the event-runner or deputy event-runner is a good idea.
* Assigning this role to someone with project management or planning experience is a great idea.
* All other team members will need to either update the project repository (issues, etc) or supply relevant status information to the project manager to facilitate free-flow of status information.
* It is recommended that every member of the team cc all planning-related correspondence to the project manager
* The project manager should check in with team members on a regular basis to collect status updates on issues and report on the project's progress (are we ahead? on schedule? behind?)

## Private or Public? ##
Some hosted repository services allow users to create private repositories. Others only allow that feature to paid subscribers.
* A private repository will hide all of the information, issues, milestones, and documents from public view, but everyone on the team will need a login account and be granted access to the project if they are to view or update project information.
* A public repository will reveal all of the information (sometimes with some exceptions) to all visitors. Team members will be able to view the information, but will not be able to make any changes or updates unless they also have accounts and are added to the project as contributors. Assuming that the project manager is the owner of the repository, they will be making all of the changes.
* A third option would be for the project manager to create a private repository, where they are the only authorized contributor, and they use presentation tools to share their screen during planning meetings so that other members of the team can view the information. Extracted status information can be emailed to the team periodically.
* Integrations can help by sending updates to a Discord or Slack channel

# Overview of Tools #
The following tools will be useful when planning an event:

* Files: store documents here (announcements, advertisements, poster designs, ticket designs, site token designs, schedules, and event booklets). Rather than saving a new copy when something changes, you will "commit" your changes directly to the originating file. You will be able to view the file's full change history and roll back changes if needed. Some files can be edited right in the web interface. Others you will need to upload or otherwise transfer in.
* Milestones: these are planning checkpoints you can use to help visualize where you are in the planning process and keep your tasks organized so you do the urgent ones first. You can set any number of milestones that you think you'll need. Example milestones are set at 1 year, 6/4/3/2 months, and 4/3/2/1 weeks. You can use the examples by updating the start/end dates for each one. Where one milestone ends, the next begins.
* Issues: This is your fancy to-do list. Every thing you have to do goes into one Issue, though you can group similar tasks under a single issue if you like. Assign an appropriate milestone to each issue, based on when each issue should be complete. If an Issue calls for creating or updating a document, you can tie them together by pasting a commit identifier (looks like alpha-numeric soup) into the issue description or a comment to the issue. You can assign an issue to someone so everyone will know that someone is working on it.
* Service Desk: each project has a dedicated email address that can be used to contact the planning team with questions. Build it into a contact form on a website to hide it from email address scrapers. Each email sent in works just like an Issue, and you can reply back to the originator to get more information. Replies will show up as comments.
* Wiki: each project comes with its own dedicated wiki. For planning an event, the wiki tool would be a good place to keep information that team members might need - links to social media posts, links to official announcement pages, contact information for the event site contact, perhaps?

## Examples ##
- [A milestone with 2 issues](https://gitlab.com/enchanted-tarantula/planning-templates/sca-event-planning-example/-/milestones/2)

## Edit this Page ##
Edit this page to provide overview information or updated "big picture" project status information. What appears here is whatever is contained within the file named "README.md", which you can edit right here (click the file name in the file list above, then select "Open in Web IDE" or choose "Edit this file only" from the same pulldown)

You can have one `README.md` file per folder, and they can all be different. README files in subfolders can be used to outline instructions for the files found in that folder. Or you can use it to keep notes. Just remember that if the repository is public, anyone can read any of the files.
