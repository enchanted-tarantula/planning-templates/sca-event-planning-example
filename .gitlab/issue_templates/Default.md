# Don't Submit Non-Example Issues Here

If you want to report a problem with this template project or suggest an enhancement to it, please do that at the ['Event Planning' Planning](https://gitlab.com/cordelya/event-planning-planning) repository, which is where we handle Issues for this repository that are not Example Issues for event planning.

Issues that are not Example Issues will be deleted without comment.

