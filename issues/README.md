# Issues for Importing

The .csv file in this folder contains the exported Issues from the Issues section for you to import into your own project's Issues section.

You will need to edit the file in the following columns: 
- URL - set each cell to the base url for your project, with `/-/issues/#` tacked on the end, preserving the issue ID number at the end (they are listed in order, starting with 1)
- Author Name - The display name of the Gitlab author (probably you)
- Author Username - The username of the Gitlab author (probably you)
- Created / Updated dates (be careful that you use the exact date/time pattern as is there now)

After you are happy with the content, you can upload the csv file into your Issues section (there's a button at the top)
