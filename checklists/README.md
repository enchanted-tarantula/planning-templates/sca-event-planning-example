# Checklists

Checklists for specific jobs or workflows can help ensure that important things aren't left undone.

Checklists in this folder are suggestions - start here and customize as needed.

Tips:

* Make a separate checklist for each event job (event steward, head cook, reservationist, class coordinator, heavy/rapier/archery/thrown tournament coordinator)
* Print out paper checklists for each position to have at the event (they may not use them, but provide them as a matter of course)
* You can add checkboxes in markdown by using one of the following patterns at the start of a line:

Doing this:
```markdown
* [ ] Checklist Item 1
* [ ] Checklist Item 2
* [x] Checklist Item 3 (completed item)

or

- [ ] Checklist Item 1
- [ ] Checklist Item 2
- [x] Checklist Item 3 (completed item)
```
Will look like this:
* [ ] Checklist Item 1
* [ ] Checklist Item 2
* [x] Checklist Item 3 (completed item)

Checkboxes in markdown (\*.md) files in the file repository will not be clickable, but checkboxes in Issues and Pull Requests (including comments/replies) *are* clickable - you can toggle the checkmark just by clicking on it.

* [ ] You can use them in unordered lists
    * [ ] You can use them in nested unordered list items
        * [ ] As many levels deep as you need
* [ ] You nest unordered list items by indenting at least two additional spaces for each level
    * [ ] However many indentation spaces you choose, the number of spaces to each indentation stop should be equal

1. [ ] You can also use them in ordered lists.
    1. [ ] And in nested ordered list items.
    2. [ ] Nested ordered list items require at least 3 spaces of indentation
2. [ ] You can't mix numerals and alpha characters in the same list


